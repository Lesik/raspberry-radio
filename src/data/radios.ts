import { Station } from "../structures/Station";

export const radios: Station[] = [
    {
        title: "BBC Radio 1",
        id: 'bbc_radio_one',
    },
    {
        title: "BBC Radio 1 Dance",
        id: 'bbc_radio_one_dance',
    },
    {
        title: "BBC Radio 2",
        id: 'bbc_radio_one_two',
    },
    {
        title: "BBC Radio 3",
        id: 'bbc_radio_three',
    },
    {
        title: "BBC Radio 4",
        id: 'bbc_radio_four',
    },
    {
        title: "BBC Radio 5 live",
        id: 'bbc_radio_five_live',
    },
    {
        title: "BBC Radio 6",
        id: 'bbc_6music',
    },
];
