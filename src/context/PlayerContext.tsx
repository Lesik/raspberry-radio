import { createContext, ComponentChildren } from "preact";
import { MediaPlayer } from 'dashjs';
import { useContext, useEffect, useRef, useState } from "preact/compat";
import { Station } from "../structures/Station";

const url = 'https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/cfs/bbc_6music.mpd';

type PlayerContextType = {
    currentStation: Station | undefined,
    isPlaying: boolean,
    playStation: (station: Station) => void,
    pause: () => void,
};

const PlayerContext = createContext({} as PlayerContextType);

export const usePlayer = () => useContext(PlayerContext);

const player = MediaPlayer().create();

export const PlayerContextProvider = ({ children }: {
    children: ComponentChildren,
}) => {
    const videoRef = useRef<HTMLVideoElement>(null);
    const [isPlaying, setIsPlaying] = useState(false);
    const [currentStation, setCurrentStation] = useState<Station>();

    const playStation = (station: Station) => {
        videoRef.current && player.initialize(
            videoRef.current,
            `https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/cfs/${station.id}.mpd`,
            true,
        );
        setIsPlaying(true);
    };

    const pause = () => {
        player.pause();
        setIsPlaying(false);
    }

    const contextValues = {
        currentStation,
        isPlaying,
        playStation: playStation,
        pause,
    };

    return (
        <>
            <video controls ref={videoRef} style={{ display: 'none' }} />
            <PlayerContext.Provider value={contextValues}>
                {children}
            </PlayerContext.Provider>
        </>
    );
};
