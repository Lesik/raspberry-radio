type AcceptableArg = undefined | string | Array<string> | Record<string, boolean | undefined>;
export function cl(...args: AcceptableArg[]): string {
    const classes: string[] = [];

    args.forEach((arg) => {
        // safeguard for undefined values
        if (typeof arg === 'undefined' || arg === null || arg === '') {
            return;
        }

        if (typeof arg === 'string') {
            classes.push(arg);
        } else if (Array.isArray(arg)) {
            classes.push(cl(...arg));
        } else if (typeof arg === 'object') {
            Object.entries(arg).forEach(([key, value]) => {
                if (value) {
                    classes.push(key);
                }
            });
        }
    });

    return classes.join(' ');
}

