import { useQuery } from "react-query";
import { Station } from "../structures/Station";
import axios from "axios";

type CurrentShowResponse = {
    titles: {
        primary: string,
        secondary: string,
        tertiary: string,
    }
    network: {
        logo_url: string,
    },
    image_url: string,
};

export const useCurrentShow = (station: Station) => useQuery(
    [
        'currentShow',
        { stationId: station.id },
    ],
    () => axios.get<CurrentShowResponse>(`https://rms.api.bbc.co.uk/v2/networks/${station.id}/playable`),
    {},
);
