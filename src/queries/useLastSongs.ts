import axios from "axios";
import { useQuery } from "react-query";
import { Station } from "../structures/Station";

type LastSongsResponse = {
    data: [] | Array<{
        id: string,
        titles: {
            primary: string,
            secondary: string,
            tertiary: string,
        },
        image_url: string,
        offset: {
            label: string,
            now_playing: boolean,
        }
    }>,
}

export const useLastSongs = (station: Station) => useQuery(
    [
        'nowPlaying',
        { stationId: station.id },
    ],
    () => axios.get<LastSongsResponse>(`https://rms.api.bbc.co.uk/v2/services/${station.id}/segments/latest`),
    {
        staleTime: 10000,
    },
);
