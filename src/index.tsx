import { PlayerContextProvider } from "./context/PlayerContext";
import { RadioPage } from "./components/RadioPage";
import { radios } from "./data/radios";
import SwipeableViews from "react-swipeable-views";
import { JSXInternal } from "preact/src/jsx";
import { QueryClient, QueryClientProvider } from "react-query";
import { SwipeContainer } from "./components/SwipeContainer";
import './index.css';

const styles: JSXInternal.CSSProperties = {
    width: 320,
    height: 480,
    fontFamily: 'sans-serif',
    border: '1px solid black',
};

const queryClient = new QueryClient();

const App = () => {
    return (
        <div style={styles}>
            <QueryClientProvider client={queryClient}>
                <PlayerContextProvider>
                    <SwipeContainer />
                </PlayerContextProvider>
            </QueryClientProvider>
        </div>
    );
};

export default App;
