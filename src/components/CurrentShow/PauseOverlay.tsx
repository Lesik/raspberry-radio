import { usePlayer } from "../../context/PlayerContext";
import pause_black from '../../assets/images/pause_24dp.svg';
import classes from './PauseOverlay.module.css';
import { cl } from "../../helpers/react";

export const PauseOverlay = () => {
    const { isPlaying } = usePlayer();


    return (
        <button
            role="button"
            title="Play/pause"
            className={cl(classes.overlay, { [classes.paused]: !isPlaying })}
        >
            <img src={pause_black} />
        </button>
    );
};
