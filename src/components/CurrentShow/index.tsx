import { Station } from "../../structures/Station";
import { useCurrentShow } from "../../queries/useCurrentShow";
import { JSXInternal } from "preact/src/jsx";
import classes from './index.module.css';
import { usePlayer } from "../../context/PlayerContext";
import { cl } from "../../helpers/react";
import { PauseOverlay } from "./PauseOverlay";
import { ShowTitle } from "./ShowTitle";
import { PlayPauseButton } from "./PlayPauseButton";

const container: JSXInternal.CSSProperties = {
    position: 'relative',
};

const stationLogo: JSXInternal.CSSProperties = {
    position: 'absolute',
    width: '25%',
    height: '25%',
    right: 0,
    bottom: 0,
};

const programmeThumbnail: JSXInternal.CSSProperties = {};

export const CurrentShow = ({ station }: { station: Station }) => {
        const query = useCurrentShow(station);
        const { isPlaying, playStation, pause } = usePlayer();
        const onClick = () => isPlaying ? pause() : playStation(station);

        if (query.isSuccess) {
            const {
                image_url,
                network: {
                    logo_url,
                },
                titles: {
                    primary,
                },
            } = query.data.data;
            return (
                <>
                    <button
                        type="button"
                        title="Play/pause"
                        onClick={onClick}
                        className={classes.container}
                    >
                        {/*<PauseOverlay />*/}
                        <img
                            className={classes.stationLogo}
                            alt={`Logo of ${station.title}`}
                            src={logo_url.replace('{type}', 'colour').replace('{size}', 'default').replace('{format}', 'svg')}
                        />
                        <img
                            style={classes.programmeThumbnail}
                            alt={`Thumbnail of ${primary}`}
                            src={image_url.replace('{recipe}', '320x320')}
                        />
                    </button>
                    <PlayPauseButton station={station} />
                    <ShowTitle station={station} />
                </>
            );
        }

        return <></>;
    }
;
