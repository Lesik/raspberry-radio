import { useCurrentShow } from "../../queries/useCurrentShow";
import { useLastSongs } from "../../queries/useLastSongs";
import { Station } from "../../structures/Station";
import classes from './ShowTitle.module.css';

type Title = { primary: string, secondary: string };

export const ShowTitle = ({ station }: {station: Station}) => {
    const currentShow = useCurrentShow(station);
    const { data: nowPlayingData } = useLastSongs(station);

    let title: Title;

    const currentSong = nowPlayingData?.data.data.find(date => date.offset.now_playing);
    if (currentSong) {
        title = currentSong.titles;
    } else if (currentShow.isSuccess) {
        title = currentShow.data.data.titles;
    } else {
        title = { primary: station.title, secondary: '' };
    }

    return (
        <div className={classes.titleContainer}>
            <h3 className={classes.primary}>{title.primary}</h3>
            <h4 className={classes.secondary}>{title.secondary}</h4>
        </div>
    );
};
