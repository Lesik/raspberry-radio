import { usePlayer } from "../../context/PlayerContext";
import classes from './PlayPauseButton.module.css';
import pause_svg from '../../assets/images/pause_24dp.svg';
import play_arrow_svg from '../../assets/images/play_arrow_24dp.svg';
import { Station } from "../../structures/Station";

export const PlayPauseButton = ({station}: {station: Station}) => {
    const { isPlaying, pause, playStation } = usePlayer();
    console.log('am I playing?', isPlaying);

    return (
        <div className={classes.container}>
            <button
                type="button"
                role="button"
                title="Play/pause button"
                onClick={() => isPlaying ? pause() : playStation(station)}
                className={classes.button}
            >
                <img
                    alt={isPlaying ? 'Pause icon' : 'Play icon'}
                    src={isPlaying ? pause_svg : play_arrow_svg}
                />
            </button>
        </div>
    );
};
