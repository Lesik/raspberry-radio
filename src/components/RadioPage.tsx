import { usePlayer } from "../context/PlayerContext";
import { Station } from "../structures/Station";
import { LastPlayed } from "./LastPlayed";
import { CurrentShow } from "./CurrentShow";

export const RadioPage = ({station}: {station: Station}) => {
    const player = usePlayer();
    return (
        <>
            <CurrentShow station={station} />
            <LastPlayed station={station} />
        </>
    );
};
