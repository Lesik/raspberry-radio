import { usePlayer } from "../context/PlayerContext";
import SwipeableViews, { OnChangeIndexCallback } from "react-swipeable-views";
import { radios } from "../data/radios";
import { RadioPage } from "./RadioPage";

export const SwipeContainer = () => {
    const { playStation } = usePlayer();

    const onChangeIndex: OnChangeIndexCallback = (index) => {
        const radio = radios[index];
        console.log('index', index, 'radio', radio);
        if (radio)
            playStation(radio);
    }

    return (
        <SwipeableViews enableMouseEvents onChangeIndex={onChangeIndex}>
            {radios.map(radio => <RadioPage station={radio} />)}
        </SwipeableViews>
    );
};
