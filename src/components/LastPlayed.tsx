import { Station } from "../structures/Station";
import { useLastSongs } from "../queries/useLastSongs";
import classes from './LastPlayed.module.css';

export const LastPlayed = ({ station }: { station: Station }) => {
    const nowPlaying = useLastSongs(station);

    if (nowPlaying.isSuccess) {
        const notCurrentlyPlaying = nowPlaying.data.data.data.filter(song => !song.offset.now_playing);

        return (
            <div className={classes.container}>
                {notCurrentlyPlaying.map(({ titles, offset }) => {
                    const { primary, secondary, tertiary } = titles;
                    return (
                        <div className={classes.song}>
                            <span>{primary} - {secondary}</span>
                            <span>{offset.label}</span>
                        </div>
                    );
                })
                }
            </div>
        );
    }

    return <p>Loading</p>;
};
